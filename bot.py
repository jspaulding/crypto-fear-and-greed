import requests
import pandas as pd
import numpy as np
import yfinance as yf
import matplotlib.pyplot as plt

r = requests.get("https://api.alternative.me/fng/?limit=0")

df = pd.DataFrame(r.json()['data'])
df.value = df.value.astype(int)
df.timestamp = pd.to_datetime(df.timestamp, unit='s')
df.set_index('timestamp', inplace=True)

# re-sort earliest to latest
df = df[::-1]

# download BTC price
df1 = yf.download("BTC-USD")[['Close']]

# when merging the index must be the same name.  Rename yfinance dataframe index to 'timestamp'
df1.index.name = 'timestamp'

# merge fng dataframe with yfinance 'Close' price dataframe
tog = df.merge(df1, on='timestamp')

# calcluate daily price change
tog['change'] = tog.Close.pct_change()

# hold BTC as long as value is above 50
tog['position'] = np.where(tog.value > 50, 1, 0)

strategy = tog.position * tog.change

# plot strategy against historical BTC
plt.figure(figsize=(20,10))
plt.plot((strategy + 1).cumprod(), label="FNG")
plt.plot((tog.change + 1).cumprod(), label="BTC")
plt.legend()